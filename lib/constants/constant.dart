import 'package:flutter/material.dart';

class AppConstant{
  static String noteTable = 'note';
  static String userTable = 'userNote';
}

// rgb(252, 99, 72, .5)
// Color.fromRGBO(24,233, 111, 0.6),
Color bgColor = const Color(0xFFFC6348);
Color gradientOneColor = const Color(0xFFFE0944);
Color gradientTwoColor = const Color(0xFFFA823B);
Color shadowColor = const Color.fromRGBO(252,99, 72, 0.3);
Color borderColor = const Color(0xFFFC6348);
Color textColorWhite = Colors.white;


double sizeFont13 = 13;
double sizeFont14 = 14;
double sizeFont15 = 15;
double sizeFont16 = 16;
double sizeFont17 = 17;
double sizeFont18 = 18;
double sizeFont20 = 20;
double sizeFont22 = 22;
double sizeFont24 = 24;
double sizeFont25 = 25;
double sizeFont30 = 30;
double sizeFont35 = 35;

double padding0 = 0;
double padding5 = 5;
double padding8 = 8;
double padding10 = 10;
double padding13 = 13;
double padding15 = 15;
double padding16 = 16;
double padding20 = 20;
double padding25 = 25;
double padding30 = 30;

double margin3 = 3;
double margin5 = 5;
double margin8 = 8;
double margin10 = 10;
double margin15 = 15;
double margin16 = 16;
double margin20 = 20;
double margin25 = 25;
double margin30 = 30;
double margin35 = 35;




TextStyle largeTextStyle = TextStyle(
    fontSize: 60,
    color: textColorWhite
);

TextStyle semiLargeTextStyle = TextStyle(
    fontSize: 35,
    color: textColorWhite
);

TextStyle mediumTextStyle = TextStyle(
  fontSize: 20,
  color: textColorWhite
);

TextStyle smallTextStyle = TextStyle(
  fontSize: 18,
  color: textColorWhite
);



TextStyle largeTextStyleBlack = const TextStyle(
    fontSize: 60,
);

TextStyle semiLargeTextStyleBlack = const TextStyle(
    fontSize: 35,
);

TextStyle mediumTextStyleBlack = const TextStyle(
  fontSize: 20,
);

TextStyle mediumTextStyleBlackWithBold = const TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.w800
);

TextStyle smallTextStyleBlack = const TextStyle(
  fontSize: 18,
);

TextStyle smallTextStyleBlackWithItalic = const TextStyle(
  fontSize: 16,
  fontStyle: FontStyle.italic
);




String searchTxt = 'Search by title(min 2 letters)';
String labelTxt = 'Search by title';



// double iconMedium = 70;
// String maleText = 'Male'.toUpperCase();
// String femaleText = 'Female'.toUpperCase();
// String heightTxt = 'HEIGHT';
// String heightUnit = 'cm';
//
//
// double circleDimension = 185;
//
// double borderRadiusSmall = 8.0;
// double marginSmall = 8.0;
// double paddingMedium = 16.0;
//
// enum Gender {
//   male,
//   female,
//   other
// }
