import 'dart:io';
import 'package:mh_notpade/constants/constant.dart';
import 'package:mh_notpade/models/notebook.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../models/notebook_user.dart';

class DatabaseHelper {
  Future<Database> initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, 'notebook.db');
    return openDatabase(path, version: 1, onCreate: (db, version) {
      db.execute(
          'CREATE TABLE ${AppConstant.noteTable} (id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT,content TEXT,date TEXT)');
      db.execute(
          'CREATE TABLE ${AppConstant.userTable} (id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,userName TEXT,password TEXT)');
    });
  }

  //users database======================

  //insert register data
  Future<int> registerUser(NoteBookUser noteBookUser) async {
    Database? db = await initDatabase();
    return db.insert(AppConstant.userTable, noteBookUser.toMap());
  }

  //fetch register data
  Future<List<NoteBookUser>> fetchUserList(
      String userName, String password) async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> userMapList = await db.rawQuery(
        "SELECT * FROM ${AppConstant.userTable} WHERE userName = '$userName' AND password = '$password'");
    return List.generate(userMapList.length, (index) {
      Map<String, dynamic> mUserList = userMapList[index];
      return NoteBookUser(
          id: mUserList['id'],
          name: mUserList['name'],
          userName: mUserList['userName'],
          password: mUserList['password']);
    });
  }

  //fetch user id for shared pref data===
  Future<List<NoteBookUser>> fetchUserById(
      int userId) async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> userMapList = await db.rawQuery(
        "SELECT * FROM ${AppConstant.userTable} WHERE id = '$userId'");
    return List.generate(userMapList.length, (index) {
      Map<String, dynamic> mUserList = userMapList[index];
      return NoteBookUser(
          id: mUserList['id'],
          name: mUserList['name'],
          userName: mUserList['userName'],
          password: mUserList['password']);
    });
  }


  //Check User Exist? or not
  Future<bool> isUserExist(String userName) async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> userMapList = await db.rawQuery(
        "SELECT * FROM ${AppConstant.userTable} WHERE userName = '$userName'");
    if (userMapList.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  //notebook database=========================

  //insert notebook data
  Future<int> insertNote(NoteBook notebook) async {
    Database? db = await initDatabase();
    return db.insert(AppConstant.noteTable, notebook.toMap());
  }

  //fetch notebook data
  Future<List<NoteBook>> fetchNoteList() async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> notebookmaplist =
        await db.query(AppConstant.noteTable);
    return List.generate(notebookmaplist.length, (index) {
      Map<String, dynamic> mNotebook = notebookmaplist[index];
      return NoteBook(
          id: mNotebook['id'],
          title: mNotebook['title'],
          content: mNotebook['content'],
          date: mNotebook['date']);
    });
  }

  Future<int> updateNote(NoteBook notebook) async {
    Database db = await initDatabase();
    return db.update(AppConstant.noteTable, notebook.toMap(),
        where: 'id = ?', whereArgs: [notebook.id]);
  }

  Future<int> deleteNote(int id) async {
    Database db = await initDatabase();
    return db.delete(AppConstant.noteTable, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> deleteTable(String tableName) async {
    Database db = await initDatabase();
    return db.delete(AppConstant.noteTable);
  }
}
