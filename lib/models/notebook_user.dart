class NoteBookUser {
  int? id;
  String? name;
  String? userName;
  String? password;

  NoteBookUser({this.id, this.name, this.userName, this.password});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'userName': userName,
      'password': password,
    };
  }
}
