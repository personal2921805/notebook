import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mh_notpade/database_helper/database_helper.dart';
import 'package:mh_notpade/models/notebook.dart';
import '../constants/constant.dart';
import '../widgets/reusabel_textfiled.dart';
import '../widgets/reusable_card.dart';

class NoteAddPage extends StatefulWidget {

  const NoteAddPage({ Key? key}) : super(key: key);

  @override
  State<NoteAddPage> createState() => _NoteAddPageState();
}

class _NoteAddPageState extends State<NoteAddPage> {
  // String? userSelectedDate;

  TextEditingController? titleTextEditingCtrl;
  TextEditingController? contentTextEditingCtrl;
  String? userSelectedDate;
  DatabaseHelper? db;

  @override
  void initState() {
    super.initState();
    titleTextEditingCtrl = TextEditingController();
    contentTextEditingCtrl = TextEditingController();
    userSelectedDate = DateTime.now().toString().substring(0,10);
    db = DatabaseHelper();
  }

  void addNoteHelper() async {
    try{
     int isInsert = await db!.insertNote(NoteBook(title: titleTextEditingCtrl!.text, content: contentTextEditingCtrl!.text, date: userSelectedDate));
     print(isInsert);
     if(isInsert>0){
       print('Problem 1');
       ScaffoldMessenger.of(context).showSnackBar(
         const SnackBar(
           content: Text(
             'Your note added successfully',
           ),
         ),
       );
       Navigator.pop(context, true);
     }else {
       ScaffoldMessenger.of(context).showSnackBar(
         const SnackBar(
           content: Text(
             'Failed to add note',
           ),
         ),
       );
     }
    }catch(error){
      if (kDebugMode) {
        print(error.toString());
      }
    }
  }
  
  
  
  
  
  
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Note'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(padding15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Add Title:-',
                style: mediumTextStyleBlack,
              ),
              ReusableCard(
                margin: EdgeInsets.only(top: margin20, bottom: margin15),
                cardChild: ReusableTextField(
                  controller: titleTextEditingCtrl,
                  minLines: 1,
                  maxLines: 5,
                  hintText: '',
                  labelText: 'Add Title',
                ),
              ),
              Text(
                'Add Description:-',
                style: mediumTextStyleBlack,
              ),
              ReusableCard(
                margin: EdgeInsets.only(top: margin20, bottom: margin15),
                cardChild: ReusableTextField(
                  controller: contentTextEditingCtrl,
                  minLines: 5,
                  maxLines: 50,
                  hintText: '',
                  labelText: 'Add Description',
                ),
              ),
              Text(
                'Add Date:-',
                style: mediumTextStyleBlack,
              ),
              ReusableCard(
                margin: EdgeInsets.only(top: margin20),
                cardChild: Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: padding15),
                  decoration: BoxDecoration(
                    border: Border.all(width: 0.7),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: ListTile(
                    contentPadding: EdgeInsets.all(padding0),
                    onTap: () {
                      showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime(2100),
                      ).then((value) {
                        setState(() {
                          userSelectedDate = value.toString().substring(0, 10);
                        });
                      });
                    },
                    title: Text(
                      userSelectedDate!,
                      style: TextStyle(
                          fontSize: sizeFont14,
                          fontStyle: FontStyle.italic,
                          color: Colors.grey.shade500),
                    ),
                    trailing: const Icon(Icons.calendar_month_sharp),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(
                      vertical: padding13, horizontal: padding25),
                ),
                onPressed: () {
                  if (titleTextEditingCtrl!.text.isEmpty) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          'Please Enter title',
                          style: smallTextStyleBlack,
                        ),
                      ),
                    );
                  } else if (contentTextEditingCtrl!.text.isEmpty) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          'Please Enter Description',
                          style: smallTextStyleBlack,
                        ),
                      ),
                    );
                  }else{
                    addNoteHelper();
                  }
                },
                child: Text(
                  'Add Note',
                  style: mediumTextStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
