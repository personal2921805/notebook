import 'package:flutter/material.dart';
import 'package:mh_notpade/constants/constant.dart';
import 'package:mh_notpade/database_helper/database_helper.dart';
import 'package:mh_notpade/models/notebook_user.dart';
import 'package:mh_notpade/util/util.dart';
import 'package:mh_notpade/widgets/reusabel_textfiled.dart';
import '../models/notebook.dart';
import '../widgets/drawer_widget.dart';
import '../widgets/reusable_card.dart';
import 'add_note_page.dart';
import 'edit_note_page.dart';

class HomePage extends StatefulWidget {
  final NoteBookUser? user;

  const HomePage({Key? key, this.user}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DatabaseHelper? _db;
  List<NoteBook>? noteBookList;
  List<NoteBook>? noteBookListContainer;
  bool? _isLoading;

  @override
  void initState() {
    super.initState();
    _db = DatabaseHelper();
    _isLoading = true;
    noteBookList = [];
    noteBookListContainer = [];
    fetchNoteList();
  }

  filterForSearching(String query) {
    // query.isEmpty [for 1 character search]
    // query.length <=1 [for 2  character search]
    if (query.length <= 1) {
      noteBookList = noteBookListContainer;
    } else {
      List<NoteBook> mList = [];
      for (NoteBook noteBook in noteBookListContainer!) {
        if (noteBook.title!.toLowerCase().contains(query)) {
          mList.add(noteBook);
        }
      }
      noteBookList = mList;
    }
    setState(() {});
  }

  void fetchNoteList() async {
    try {
      List<NoteBook> mNoteBookList = await _db!.fetchNoteList();
      if (mNoteBookList.isNotEmpty) {
        setState(() {
          noteBookList = mNoteBookList;
          noteBookListContainer = mNoteBookList;
        });
      }
    } catch (error) {
      print(error.toString());
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading == false
        ? Scaffold(
            floatingActionButton: Container(
              margin: const EdgeInsets.only(bottom: 15, right: 12),
              child: FloatingActionButton(
                onPressed: () async {
                  bool? isAdded = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) {
                    return const NoteAddPage();
                  }));
                  if (isAdded != null && isAdded == true) {
                    setState(() {
                      _isLoading = true;
                      noteBookList = [];
                      noteBookListContainer = [];
                      fetchNoteList();
                    });
                  }
                },
                child: const Icon(Icons.add),
              ),
            ),
            appBar: AppBar(
              title: Text(
                'MH NOTEBOOK',
                style: mediumTextStyle,
              ),
            ),
            drawer: const DrawerWidgetCustom(),
            body: Padding(
              padding: EdgeInsets.all(padding15),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Icon(Icons.menu_book_sharp),
                      const SizedBox(
                        width: 3,
                      ),
                      Text(
                        'Hello ${widget.user!.name ?? ''}, ',
                        style: smallTextStyleBlack,
                      ),
                      Text(
                        '${Utils.greeting()},',
                        style: smallTextStyleBlack,
                      ),
                    ],
                  ),
                  ReusableCard(
                      margin: EdgeInsets.only(top: margin20, bottom: 15),
                      cardChild: ReusableTextField(
                        hintText: searchTxt,
                        labelText: labelTxt,
                        onChanged: (query) {
                          filterForSearching(query);
                        },
                      )),
                  Expanded(
                    child: ListView.separated(
                        separatorBuilder: (BuildContext context, int index) {
                          return const SizedBox(
                            height: 8,
                          );
                        },
                        // scrollDirection: Axis.vertical,
                        // shrinkWrap: true,
                        itemCount: noteBookList!.length,
                        itemBuilder: (BuildContext context, int index) {
                          NoteBook myNoteBook = noteBookList![index];

                          return ReusableCard(
                            cardChild: Padding(
                              padding: EdgeInsets.all(padding15),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          myNoteBook.title ?? '',
                                          style: mediumTextStyleBlackWithBold,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        const SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          myNoteBook.content ?? '',
                                          style: smallTextStyleBlack,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        const SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          myNoteBook.date ?? '',
                                          style: smallTextStyleBlackWithItalic,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  ),
                                  PopupMenuButton<String>(
                                    itemBuilder: (BuildContext context) {
                                      return <PopupMenuEntry<String>>[
                                        PopupMenuItem(
                                            value: 'key_edit',
                                            child: Row(
                                              children: const [
                                                Icon(Icons.edit_sharp),
                                                SizedBox(
                                                  width: 3,
                                                ),
                                                Text('Edit'),
                                              ],
                                            )),
                                        PopupMenuItem(
                                            value: 'key_delete',
                                            child: Row(
                                              children: const [
                                                Icon(Icons.delete_sharp),
                                                SizedBox(
                                                  width: 3,
                                                ),
                                                Text('Delete'),
                                              ],
                                            ))
                                      ];
                                    },
                                    padding: EdgeInsets.zero,
                                    icon: const Icon(Icons.more_vert_sharp),
                                    onSelected: (String value) async {
                                      if (value == "key_edit") {
                                        //do edit
                                        Navigator.push(context, MaterialPageRoute(builder: (context){
                                          return NoteEditPage(
                                          noteBook: myNoteBook,
                                          );
                                        }));
                                      } else if (value == 'key_delete') {
                                        //do delete
                                        int value = await _db!
                                            .deleteNote(myNoteBook.id ?? 0);
                                        if (value > 0) {
                                          noteBookList = [];
                                          noteBookListContainer = [];
                                          _isLoading = true;
                                          fetchNoteList();
                                        }
                                      }
                                    },
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ),
          )
        : const Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }
}
