import 'package:flutter/material.dart';
import 'package:mh_notpade/constants/constant.dart';
import 'package:mh_notpade/database_helper/database_helper.dart';
import 'package:mh_notpade/models/notebook_user.dart';
import 'package:mh_notpade/pages/home_page.dart';
import 'package:mh_notpade/pages/registration_page.dart';
import 'package:mh_notpade/util/pref_managent.dart';
import 'package:mh_notpade/widgets/reusabel_textfiled.dart';
import 'package:mh_notpade/widgets/reusabel_textfiled_password.dart';
import 'package:mh_notpade/widgets/reusable_card.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({Key? key}) : super(key: key);

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  TextEditingController? emailTxtCtrl;
  TextEditingController? passwordTxtCtrl;
  DatabaseHelper? db;

  @override
  void initState() {
    super.initState();
    emailTxtCtrl = TextEditingController();
    passwordTxtCtrl = TextEditingController();
    db = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.all(padding15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ReusableCard(
                margin: EdgeInsets.only(bottom: margin25),
                cardChild: ReusableTextField(
                  keyBordType: TextInputType.emailAddress,
                  controller: emailTxtCtrl,
                  hintText: 'Enter Your Email',
                  labelText: 'Enter Your Email',
                ),
              ),
              ReusableCard(
                  margin: EdgeInsets.only(bottom: margin30),
                  cardChild: ReusableTextFieldPassword(
                    hintText: 'Enter your Password',
                    labelText: 'Enter your Password',
                    controller: passwordTxtCtrl,
                  )),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(
                      vertical: padding16, horizontal: padding30),
                ),
                onPressed: () async {
                  if(emailTxtCtrl!.text.isNotEmpty && passwordTxtCtrl!.text.isNotEmpty){
                    List<NoteBookUser> userList = await db!.fetchUserList(emailTxtCtrl!.text, passwordTxtCtrl!.text);
                    if(userList.isNotEmpty){
                      PrefManagement.setIsLoggedIn(true);
                      PrefManagement.setUserId(userList[0].id??0);
                      Navigator.push(context, MaterialPageRoute(builder: (context){
                        return HomePage(user: userList[0],);
                      }));
                    }else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Incorrect user credential')),
                      );
                    }
                  }else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Email or password is empty')),
                    );
                  }
                },
                child: Text(
                  'Login',
                  style: mediumTextStyle,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return const RegistrationPage();
                  }));
                },
                child: Text(
                  'New to app? Please Register',
                  style: smallTextStyleBlack,
                ),
              ),
            ],
          ),
        ));
  }
}
