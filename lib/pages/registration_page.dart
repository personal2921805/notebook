import 'package:flutter/material.dart';
import 'package:mh_notpade/constants/constant.dart';
import 'package:mh_notpade/database_helper/database_helper.dart';
import 'package:mh_notpade/models/notebook_user.dart';
import 'package:mh_notpade/widgets/reusabel_textform.dart';
import 'package:mh_notpade/widgets/reusable_card.dart';

import '../widgets/reusabel_textformfiled_password.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  TextEditingController? emailTxtCtrl;
  TextEditingController? passwordTxtCtrl;
  TextEditingController? nameTxtCtrl;
  GlobalKey<FormState>? formKey;
  DatabaseHelper? db;

  @override
  void initState() {
    super.initState();
    emailTxtCtrl = TextEditingController();
    passwordTxtCtrl = TextEditingController();
    nameTxtCtrl = TextEditingController();
    formKey = GlobalKey<FormState>();
    db = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.all(padding15),
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ReusableCard(
                  margin: EdgeInsets.only(bottom: margin25),
                  cardChild: ReusableTextFormField(
                    keyBordType: TextInputType.text,
                    validator: (String? value) {
                      if (value!.trim().isEmpty) {
                        return 'Enter your Name';
                      } else {
                        return null;
                      }
                    },
                    controller: nameTxtCtrl,
                    hintText: 'Enter Your Name',
                    labelText: 'Enter Your Name',
                  ),
                ),
                ReusableCard(
                  margin: EdgeInsets.only(bottom: margin25),
                  cardChild: ReusableTextFormField(
                    validator: (String? value) {
                      if (value!.trim().isEmpty) {
                        return 'Enter your Email';
                      } else {
                        return null;
                      }
                    },
                    keyBordType: TextInputType.emailAddress,
                    controller: emailTxtCtrl,
                    hintText: 'Enter Your Email',
                    labelText: 'Enter Your Email',
                  ),
                ),
                ReusableCard(
                    margin: EdgeInsets.only(bottom: margin30),
                    cardChild: ReusableTextFormFieldPassword(
                      validator: (String? value) {
                        if (value!.trim().isNotEmpty && value.length >= 5) {
                          return null;
                        } else {
                          return 'Enter your valid password (min 4)';
                        }
                      },
                      keyBordType: TextInputType.visiblePassword,
                      hintText: 'Enter your Password',
                      labelText: 'Enter your Password (min 4)',
                      controller: passwordTxtCtrl,
                    )),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(
                        vertical: padding16, horizontal: padding30),
                  ),
                  onPressed: () async {
                    if (formKey!.currentState!.validate()) {
                      bool isExist = await db!.isUserExist(emailTxtCtrl!.text);
                      if (isExist) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text('${emailTxtCtrl!.text} Already Exist'),
                        ));
                      } else {
                        NoteBookUser noteBookUser = NoteBookUser(
                          name: nameTxtCtrl!.text,
                          password: passwordTxtCtrl!.text,
                          userName: emailTxtCtrl!.text,
                        );
                        int? register = await db!.registerUser(noteBookUser);

                        if (register > 0) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content:
                                Text('${emailTxtCtrl!.text} Successfully Registered'),
                          ));
                          Navigator.pop(context);
                        }
                      }
                    }
                  },
                  child: Text(
                    'Register',
                    style: mediumTextStyle,
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Already Registered? Please login',
                    style: smallTextStyleBlack,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
