import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mh_notpade/constants/constant.dart';
import 'package:mh_notpade/database_helper/database_helper.dart';
import 'package:mh_notpade/models/notebook_user.dart';
import 'package:mh_notpade/pages/login_page.dart';
import 'package:mh_notpade/util/pref_managent.dart';
import 'home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    initSharedPref();
    setState(() {
    });
  }

  initSharedPref() async{
    await PrefManagement.init();
    reDirectToPage();

  }

  void reDirectToPage() {
    Timer(const Duration(seconds: 3), () async {
      if (PrefManagement.getIsLoggedIn() != null &&
          PrefManagement.getIsLoggedIn() == true) {
        DatabaseHelper db = DatabaseHelper();
        List<NoteBookUser> userList =
            await db.fetchUserById(PrefManagement.getUserId() ?? 0);
        print(userList[0]);
        if (userList.isNotEmpty) {
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) {
            return  HomePage(
              user: userList[0],
            );
          }), (route) => false);
        } else {
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) {
            return const LogInPage();
          }), (route) => false);
        }
      } else {
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) {
          return const LogInPage();
        }), (route) => false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [gradientOneColor, gradientTwoColor],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: const [0.15, 0.55],
            tileMode: TileMode.repeated,
          ),
        ),
        width: double.maxFinite,
        // color: bgColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/notebook.png',
              height: 100,
            ),
            Text(
              'MH NoteBook',
              style: semiLargeTextStyle,
            ),
            const SizedBox(
              width: 215,
              height: 3,
              child: LinearProgressIndicator(
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}
