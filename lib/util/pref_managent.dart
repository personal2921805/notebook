import 'package:shared_preferences/shared_preferences.dart';

class PrefManagement {
  static SharedPreferences? pref;
  static const String _userId = 'userId';
  static const String _isLoggedIn = 'isLoggedIn';

  static Future<void> init() async {
    pref ??= await SharedPreferences.getInstance();
  }

  static void setUserId(int id) {
    pref!.setInt(_userId, id);
  }

  static int? getUserId(){
    return pref!.getInt(_userId);
  }

  static void setIsLoggedIn(bool value) {
    pref!.setBool(_isLoggedIn, value);
  }

  static bool? getIsLoggedIn(){
    return pref!.getBool(_isLoggedIn);
  }

  static void prefClear(){
    pref!.clear();
  }




}
