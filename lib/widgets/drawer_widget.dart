import 'package:flutter/material.dart';
import 'package:mh_notpade/util/pref_managent.dart';
import '../constants/constant.dart';
import '../pages/login_page.dart';

class DrawerWidgetCustom extends StatelessWidget {
  const DrawerWidgetCustom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.red,
            ),
            child: Text(
              'Drawer Header',
              style: mediumTextStyle,
            ),
          ),
          ListTile(
            leading: const Icon(Icons.help_sharp),
            title: const Text('Help Desk'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.logout_sharp),
            title: const Text('LogOut'),
            onTap: () {
              PrefManagement.prefClear();
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) {
                return const LogInPage();
              }), (route) => false);
            },
          ),
        ],
      ),
    );
  }
}
