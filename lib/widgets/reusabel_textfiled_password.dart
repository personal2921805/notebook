import 'package:flutter/material.dart';
import '../constants/constant.dart';

class ReusableTextFieldPassword extends StatelessWidget {
  final String? hintText;
  final String? labelText;
  final TextInputType? keyBordType;
  final Function(String)? onChanged;
  final TextEditingController? controller;

  const ReusableTextFieldPassword({super.key, required this.hintText , required this.labelText, this.controller, this.onChanged, this.keyBordType});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onChanged: onChanged,
      obscureText: true,
      keyboardType: keyBordType,
      autofocus: false,
      decoration: InputDecoration(
        labelText: labelText,
        hintText: hintText,
        floatingLabelStyle:
        TextStyle(fontSize: sizeFont16, color: bgColor),
        labelStyle: TextStyle(fontSize: sizeFont14, color: Colors.grey),
        contentPadding: EdgeInsets.symmetric(
            vertical: padding16, horizontal: padding20),
        alignLabelWithHint: true,
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: borderColor,
            width: 0.7,
          ),
        ),
        border: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
            width: 0.7,
          ),
        ),
      ),
    );
  }
}
