import 'package:flutter/material.dart';
import '../constants/constant.dart';

class ReusableTextFormField extends StatelessWidget {
  final String? hintText;
  final String? labelText;
  final TextInputType? keyBordType;
  final FormFieldValidator<String>? validator;
  final TextEditingController? controller;

  const ReusableTextFormField({super.key, required this.hintText , required this.labelText, this.controller, this.validator, this.keyBordType});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: validator,
      keyboardType: keyBordType,
      autofocus: false,
      decoration: InputDecoration(
        labelText: labelText,
        hintText: hintText,
        floatingLabelStyle:
        TextStyle(fontSize: sizeFont16, color: bgColor),
        labelStyle: TextStyle(fontSize: sizeFont14, color: Colors.grey),
        contentPadding: EdgeInsets.symmetric(
            vertical: padding16, horizontal: padding20),
        alignLabelWithHint: true,
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: borderColor,
            width: 0.7,
          ),
        ),
        border: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
            width: 0.7,
          ),
        ),
      ),
    );
  }
}
