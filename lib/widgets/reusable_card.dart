import 'package:flutter/material.dart';
import '../constants/constant.dart';

class ReusableCard extends StatelessWidget {
  final Widget? cardChild;
  final EdgeInsets? margin;
  const ReusableCard({super.key, required this.cardChild, this.margin});

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: margin,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        shadowColor: shadowColor,
        borderOnForeground: true,
        elevation: 7,
        child: cardChild);
  }
}
